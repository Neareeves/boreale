/*eslint-env browser*/

// Variables
var submitBtn = gEName('submit-btn')[0];
var mail = gEName('mail')[0];
var fname = gEName('firstname')[0];
var lname = gEName('lastname')[0];
var errors = 0;


// Functions

function validateEmail(email) {
	var re = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
	return re.test(email);
}

function validateString(str) {
	var re = /^(?=.*[A-Z])(?=.{2,})/i;
	return re.test(str);
}

function submitForm(e) {
	e.preventDefault();
	errors = 0;
	var errorMail = '';
	if (mail.value == '') {
		errorMail = 'Veuillez renseigner un email.';
	}
	else {
		if (validateEmail(mail.value) === false) {
			errorMail = 'Votre email n\'est pas valide.';
		}
		else existEmail();
	}
	gEId('error-mail').innerHTML = errorMail;

	var errorFname = '';
	if (validateString(fname.value) === false || fname.value.length > 50) {
		errorFname = 'Prénom invalide.';
		errors++;
	}
	gEId('error-fname').innerHTML = errorFname;

	var errorLname = '';
	if (validateString(lname.value) === false || lname.value.length > 50) {
		errorLname = 'Nom invalide.';
		errors++;
	}
	gEId('error-lname').innerHTML = errorLname;
}

function existEmail() {
	var request;
  if (window.XMLHttpRequest)
		request = new XMLHttpRequest();
  else
    request = new ActiveXObject('Microsoft.XMLHTTP');

  request.open('POST', '/verification-email', true);
  request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	request.setRequestHeader('X-CSRF-TOKEN', getToken());
  request.responseType = 'text';
  var data =
    'data-mail='+mail.value;
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var answer = this.responseText;
      if (answer === 'true') {
				var errorMail = 'L\'adresse email est déjà utilisée';
				gEId('error-mail').innerHTML = errorMail;
      }
      else {
				submitForm2();
      }
    }
    else if (this.readyState == 4 && this.status != 200) {
      alert('erreur '+this.status);
    }
  }
  request.send(data);
}

function submitForm2() {
	if (errors === 0) {
		gEId('admin-creation').submit();
	}
}

function getToken() {
    var meta = gEName('csrf-token')[0];
    return meta.getAttribute('content');
}


// Event listeners

submitBtn.addEventListener('click', submitForm);

// Code to execute

