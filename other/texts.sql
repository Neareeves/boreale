-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 30 jan. 2019 à 12:37
-- Version du serveur :  5.7.21
-- Version de PHP :  7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `boreale`
--

-- --------------------------------------------------------

--
-- Structure de la table `texts`
--

DROP TABLE IF EXISTS `texts`;
CREATE TABLE IF NOT EXISTS `texts` (
  `id_text` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `page` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_text`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `texts`
--

INSERT INTO `texts` (`id_text`, `title`, `text`, `page`) VALUES
(3, '0', 'Estime-toi !', 'home'),
(4, '1', 'La beauté n\'est pas futile, elle est vitale.\r\nVitale pour affronter les traitements. \r\nVitale pour guérir.\r\nBoréale vous met en relation avec des socio-esthéticiennes \r\nqui vont aideront à retrouver votre identité \r\net votre confiance en vous.', 'home'),
(5, '2', 'Boréale vous propose', 'home'),
(6, '3', 'Des prestations esthétiques adaptées à vos besoins, à votre condition, à votre maladie avec des socio-esthéticiennes diplomées.', 'home'),
(7, '4', 'Des séances particulières avec un praticien \r\nde l\'esthétique réapprendre \r\nà prendre soin de vous et composer  \r\navec la nouvelle version de votre corps.', 'home'),
(8, '5', 'Des ateliers pour apprendre, en groupe, à camoufler les vestiges de la maladie.', 'home'),
(9, '6', 'Des produits pensés pour les corps éprouvés', 'home'),
(11, '0', 'Qualité, Sécurité, Compétence', 'patient-infos'),
(12, '1', 'Nos praticiennes sont toutes diplômées\r\nd’État et ont reçu une formation de\r\nspécialisation en socio-esthétique ou\r\nonco-esthétique.', 'patient-infos'),
(13, '2', 'Elles s’engagent à respecter une charte\r\nd’éthique et un contrat de confidentialité \r\nprotégeant vos informations privées.', 'patient-infos'),
(14, '3', 'Les praticiennes qui travaillent avec Boréale s\'investissent de façon constante, se forment aux dernières techniques et nouveautés en matière de socio-esthétique.', 'patient-infos'),
(15, '4', 'Avec Boréale pas d\'échange d\'argent avec\r\nla praticienne. Vous payez en ligne, ce qui\r\nlibère votre relation de confiance avec\r\nvotre accompagnatrice.\r\nVous pouvez régler en carte bancaire ou\r\nutiliser vos CESU.\r\nUne autorisation d\'agrément Sécurité\r\nSociale est encours.', 'patient-infos'),
(17, '0', 'Boréale réunit l\'humain et la confiance en soi', 'practitioner-infos'),
(18, '1', 'Vous avez une vocation, Boréale vous offre\r\nun revenu.\r\nVous entrez en relation avec des personnes\r\nqui ont besoin de vous.', 'practitioner-infos'),
(19, '2', 'Boréale vous permet d\'organiser votre emploi du temps et d\'automatiser la prise de rendez-vous de vos clients.\r\nSa plateforme de paiement,sécurise votre rémunération.', 'practitioner-infos'),
(20, '3', 'Vous pouvez échanger avec la communauté de socio-esthéticiennes sur nos réseaux sociaux et partager vos réalisations grâce aux témoignages du bien-être apporté à vos clients.', 'practitioner-infos'),
(24, '0', 'Bienvenue dans votre profil, ici vous pouvez faire pleinde choses très intéressantes', 'profile'),
(33, '0', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor', 'about-us'),
(34, '1', 'Qui sommes-nous ?', 'about-us'),
(35, '2', 'Découvrir Boréale', 'about-us'),
(36, '3', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.', 'about-us'),
(37, '4', 'Boréale vous propose', 'about-us'),
(38, '5', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.', 'about-us'),
(39, '6', 'Nos valeurs et engagements', 'about-us'),
(40, '7', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.', 'about-us'),
(41, '0', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium', 'legal'),
(42, '1', 'Mentions Légales', 'legal'),
(43, '2', 'Sed ut perspiciatis unde omnis ', 'legal'),
(44, '3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'legal'),
(45, '4', 'Sed ut perspiciatis unde omnis ', 'legal'),
(46, '5', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'legal'),
(48, '0', 'At vero eos et accusamus et iusto odio dignissimos ducimus', 'partner'),
(49, '1', 'Nos partenaires', 'partner'),
(50, '0', 'Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates', 'faq'),
(51, '1', 'Foire Aux Questions', 'faq'),
(52, '0', 'Nous contacter', 'contact'),
(53, '2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit ?', 'faq'),
(54, '3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'faq'),
(55, '4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ?', 'faq'),
(56, '5', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'faq'),
(57, '6', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit ?', 'faq'),
(58, '7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'faq'),
(59, '8', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ?', 'faq'),
(60, '9', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'faq'),
(61, '10', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do?', 'faq'),
(62, '11', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'faq'),
(63, '0', 'Bienvenue dans votre espace de recherche, ici vous trouverez les prestations de vos choix', 'prestations'),
(64, '1', 'Trouver votre prestation', 'prestations'),
(65, '1', 'Votre profil', 'profile'),
(66, '0', 'Bienvenue dans votre espace, ici vous trouverez tout ce dont vous avez besoin', 'patient-area'),
(67, '0', 'Bienvenue dans votre espace, ici vous trouverez tout ce dont vous avez besoin', 'practitioner-area'),
(68, '0', 'Bienvenue dans votre espace, ici vous trouverez tout ce dont vous avez besoin', 'admin-area'),
(69, '0', 'Bienvenue dans votre agenda, ici vous pouvez visualiser vos prochains rendez-vous', 'agenda'),
(70, '1', 'Vos rendez-vous', 'agenda'),
(71, '2', 'Nom du partenaire', 'partner'),
(72, '3', 'Présentation du partenaire : Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'partner'),
(73, '0', 'Bienvenue dans votre espace d\'édition de pages, ici vous pouvez accéder aux pages du site dont vous souhaitez modifier le contenu', 'edition'),
(74, '0', 'Bienvenue dans votre espace de gestion de comptabilité', 'practitioner-accounting'),
(75, '0', 'Bienvenue dans votre espace de gestion de vos prestations', 'practitioner-services');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
