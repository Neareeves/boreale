-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mer. 23 jan. 2019 à 15:58
-- Version du serveur :  10.1.30-MariaDB
-- Version de PHP :  7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `boreale`
--

-- --------------------------------------------------------

--
-- Structure de la table `mails`
--

CREATE TABLE `mails` (
  `type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `mails`
--

INSERT INTO `mails` (`type`, `subject`, `message`) VALUES
('patient-validation', 'Boréale: création de compte', 'bonjour, nous sommes très heureux de vous compter parmi nous.'),
('practitioner-validation', 'Boréale: création de compte', 'bonjour, nous sommes très heureux de vous compter à présent parmi nous'),
('admin-validation', 'Boréale: création de compte', 'bonjour, vous avez été choisi pour rejoindre léquipe de Boréale. nous sommes très heureux de vous compter à présent parmi nous. Cordialement, le super administrateur.'),
('contact', 'Boréale: contact', 'Nous avons bien reçu votre message, et nous nous efforcerons de vous donner une réponse dans les plus bref délais.\nAu revoir'),
('lost-password', 'mot de passe perdu', 'Vous avez demandé la réinitialisation du mot de passe de votre compte Boréale associé à cette adresse mail.\nCliquez sur le lien suivant ou copiez-le dans votre navigateur pour finir la démarche.\n\nSi vous n&#39;avez pas demandé la réinitialisation de votre mot de passe, veuillez ne pas tenir compte de ce mail.');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
