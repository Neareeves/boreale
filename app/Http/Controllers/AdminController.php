<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Redirector;
use App\Http\Controllers\Controller;

class AdminController extends Controller {
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function adminDelete(){
		if (session('super')) {
			$result = displayAdmin();
			$idA = Input::get('id');
			if($idA == 1){
				return view('error', ['errors' => ['Cet administrateur ne peut pas être supprimé.']]);
			}
			$idU = getId($idA, 'admin');

			$delete = removeAdmin($idA, $idU);

		return redirect()->to('/espace-admin/admins')->send();

		}
		abort(403, 'Vous n\'avez pas le rang nécessaire pour voir cette page !');
	}

	public function adminArea() {
		if (session('type') === 'admin'){
			$texts = getTexts('admin-area');
			return view('admin-area', ['texts' => $texts]);
		}
		abort(403, 'Vous n\'avez pas le rang nécessaire pour voir cette page !');
	}

	public function adminAreaAdmins() {
		if (session('super')) {
			$result = displayAdmin();

			return view('admin-area-admins', ['result' => $result]);
		}
		abort(403, 'Vous n\'avez pas le rang nécessaire pour voir cette page !');
	}

	public function adminCreate(){
		return view('admin-area-create');
	}

	public function pagesEdition() {
		if (session('type') === 'admin') {
			$texts = getTexts('edition');
			return view('edition', ['texts' => $texts]);
		}
		abort(403, 'Vous n\'avez pas le rang nécessaire pour voir cette page !');
	}

	public function adminAreaAccounting() {
		if (session('type') === 'admin') {
			return view('admin-area-accounting');
		}
		abort(403, 'Vous n\'avez pas le rang nécessaire pour voir cette page !');
	}

	public function adminMails() {
		if (session('type') === 'admin') {
			$mail = getMail('patient-validation');
			return view('admin-area-mails', ['mail' => $mail]);
		}
		abort(403, 'Vous n\'avez pas le rang nécessaire pour voir cette page !');
	}

	public function adminServices() {
		if (session('type') === 'admin') {
			$services = services();

			return view('admin-services', ['services' => $services]);
		}
		abort(403, 'Vous n\'avez pas le rang nécessaire pour voir cette page !');
	}

	public function adminAddService() {
		if (session('type') === 'admin') {
			return view('admin-add-service');
		}
		abort(403, 'Vous n\'avez pas le rang nécessaire pour voir cette page !');
	}

	public function adminEditService() {
		if (session('type') === 'admin') {
			$id = Input::get('id');
			$service = service($id);

			return view('admin-edit-service', ['service' => $service]);
		}
		abort(403, 'Vous n\'avez pas le rang nécessaire pour voir cette page !');
	}

	public function createAdmin(){
		if(session('super')){
			if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			// retrieve data
			$accountType = 'admin';
			$mail = Input::get('mail');
			$password = generatePass();
			$cpassword = $password;
			$fname = Input::get('firstname');
			$lname = Input::get('lastname');

			// sanitize data
			$accountType = filter_var($accountType, FILTER_SANITIZE_STRING);
			$mail = filter_var($mail, FILTER_SANITIZE_STRING);
			$password = filter_var($password);
			$cpassword =  filter_var($cpassword);
			$fname = filter_var($fname, FILTER_SANITIZE_STRING);
			$lname = filter_var($lname, FILTER_SANITIZE_STRING);

			// Initializing variables
			$errorMsg = [];
			$errors = 0;

			// Data validation
			if (!preg_match('/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i', $mail)) {
				$errors++;
				array_push($errorMsg, 'Votre adresse email n\'est pas valide.');
			}
			else {
				$exist = exist($mail);
				if ($exist === true) {
					$errors++;
					array_push($errorMsg, 'L\'adresse mail saisie existe déjà');
				}
			}
			if ($password !== $cpassword) {
				$errors++;
				array_push($errorMsg, 'Vos mots de passe ne sont pas identiques.');
			}
			if (!preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$-.:-?{-~!"^_`\[\]\/])(?=.{8,})/',
				$cpassword)) {
				$errors++;
				array_push($errorMsg, 'Votre mot de passe doit contenir au moins 8
					caractères, dont une majuscule, une minuscule, un chiffre et un
					symbole.');
			}
			if (!preg_match('/^(?=.*[A-Z])(?=.{2,})/i', $fname) ||
				strlen($fname) > 50) {
				$errors++;
				array_push($errorMsg, 'Prénom invalide');
			}
			if (!preg_match('/^(?=.*[A-Z])(?=.{2,})/i', $lname) ||
				strlen($lname) > 50) {
				$errors++;
				array_push($errorMsg, 'Nom invalide');
			}
			if ($errors === 0) {

				// Hash password
				$password = password_hash($password, PASSWORD_BCRYPT);

				// Insert new account into DB
				$random = generateLink();
				insertAccount($accountType, $mail, $password, $fname, $lname, $random);

				$mailType = $accountType.'-validation';  //get appropriated mail
				$baseMail = getMail($mailType);
				$message = $baseMail['message'];

				$link = './oubli/'.$random;

				$message = '<html><head></head><body>'.$message .'<br>
											<a href="'.$link.'">
												<button style="padding:6px 10px;font-size:18px;color:#EEE;background-color:#864;">
												Changer mon mot de passe</button>
											</a>
										</body></html>';

				$message = wordwrap($message, 70);
				$subject = $baseMail['object'];

				$headers = "From: " .'cantin.p@codeur.online'. "\r\n";
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

				mail($mail, $subject, $message, $headers);
				return redirect()->to('/espace-admin/admins')->send();
			}
			return view('error', ['errors' => $errorMsg]);
		}
		return redirect()->to('/espace-admin/admins')->send();
		}
	}

	public function adminInsertService(Request $request) {
		$title = Input::get('title');
		$desc = Input::get('description');
		$price = Input::get('price');

		$errorMsg = [];
		$errors = 0;

		if (strlen($title) === 0) {
			array_push($errorMsg, 'Veuillez renseigner un titre de prestation.');
			$errors++;
		}
		if (strlen($desc) === 0) {
			array_push($errorMsg, 'Veuillez renseigner une description de prestation.');
			$errors++;
		}
		if (strlen($price) === 0) {
			array_push($errorMsg, 'Veuillez renseigner un prix de prestation.');
			$errors++;
		}

		$dir = 'img/services/';
		$pic_name = '';

		if ($request->hasFile('picture')) {
			$pic_name = $request->file('picture')->getClientOriginalName();
			$pic = $request->file('picture');

			$info = getimagesize($pic);
			if ($info !== FALSE) {
				move_uploaded_file($pic, $dir.$pic_name);
			} else {
				array_push($errorMsg, 'Le fichier n\'est pas une image.');
				$errors++;
			}
		}

		if ($errors !== 0) {
			return view('error', ['errors' => $errorMsg]);
		}

		$title = filter_var($title, FILTER_SANITIZE_STRING);
		$desc = filter_var($desc, FILTER_SANITIZE_STRING);
		$price = (float)filter_var($price, FILTER_SANITIZE_STRING);

		insertService($title, $price, $desc, $pic_name);

		return redirect()->to('/espace-admin/prestations')->send();
	}

	public function adminUpdateService(Request $request) {
		$id = Input::get('id');
		$title = Input::get('title');
		$desc = Input::get('description');
		$price = Input::get('price');

		$errorMsg = [];
		$errors = 0;

		if (strlen($title) === 0) {
			array_push($errorMsg, 'Veuillez renseigner un titre de prestation.');
			$errors++;
		}
		if (strlen($desc) === 0) {
			array_push($errorMsg, 'Veuillez renseigner une description de prestation.');
			$errors++;
		}
		if (strlen($price) === 0) {
			array_push($errorMsg, 'Veuillez renseigner un prix de prestation.');
			$errors++;
		}

		$dir = 'img/services/';
		$pic_name = service($id)->picture;

		if ($request->hasFile('picture')) {
			$pic_name = $request->file('picture')->getClientOriginalName();
			$pic = $request->file('picture');

			$info = getimagesize($pic);
			if ($info !== FALSE) {
				move_uploaded_file($pic, $dir.$pic_name);
			} else {
				array_push($errorMsg, 'Le fichier n\'est pas une image.');
				$errors++;
			}
		}

		if ($errors !== 0) {
			return view('error', ['errors' => $errorMsg]);
		}

		$title = filter_var($title, FILTER_SANITIZE_STRING);
		$desc = filter_var($desc, FILTER_SANITIZE_STRING);
		$price = (float)filter_var($price, FILTER_SANITIZE_STRING);

		updateService($id, $title, $price, $desc, $pic_name);

		return redirect()->to('/espace-admin/prestations')->send();
	}

	public function adminDeleteService() {
		if (session('type') === 'admin') {
			$id = Input::get('id');
			deleteService($id);

			return redirect()->to('/espace-admin/prestations')->send();

		} else {
			abort(403, 'Vous n\'avez pas le rang nécessaire pour voir cette page !');
		}
	}

	public function updateText() {

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {

			// sanitize data
			$text = filter_var($_POST['data-text'], FILTER_SANITIZE_STRING);
			$page = filter_var($_POST['data-page'], FILTER_SANITIZE_STRING);
			$title = filter_var($_POST['data-title'], FILTER_SANITIZE_STRING);

			$page = str_replace(
				['infos-patient', 'infos-praticien', 'profil'],
				['patient-infos', 'practitioner-infos', 'profile'],
				$page);

			if ($title == 'website-name')
				$page = 'website-name';

			updateText($text, $page, $title);
			return $text;
		}
	}

}
