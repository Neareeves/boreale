<?php


// functions

function generateLink() {
	$alpha = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

	$str = '';
	for ($i=0; $i<32; $i++)
	{
		$x = random_int(0,35);
		$str .= $alpha[$x];
	}
	return $str;
}

function generatePass() {

	$str = '';
	for ($i=0; $i <= 1; $i++) {

		//get random char by ASCII code
		$str .= chr(random_int(65,90));  // uppercase letter

		$str .= chr(random_int(48,57)); // digit
		$str .= chr(random_int(97,122)); // lowercase letter

		$str .= chr(random_int(33,47)); // some special characters
	}
	return $str;
}

function bcmode($check, $y) {
	// how many numbers to take at once? carefull not to exceed (int)
	$take = 5;
	$mod = '';
	do
	{
			$a = (int)$mod.substr( $check, 0, $take );
			$check = substr( $check, $take );
			$mod = $a % $y;
	}
	while ( strlen($check) );

	return (int)$mod;
}

function isSiret($siret) {

	$sum=0;
	if(preg_match('/^[0-9]{14}$/i', $siret)){
		for ($index = 0; $index < 14; $index ++)
		{
			$number = (int) $siret[$index];
			if (($index % 2) == 0) { if (($number *= 2) > 9) $number -= 9; }
			$sum += $number;
		}

		// Number is valid if multiple of 10
		if (($sum % 10) != 0){
			 return false;
		}
		return true;
	}
}

function verifiateIban($iban) {

/*Validation rules by country*/
			static $rules = array(
				'AL'=>'[0-9]{8}[0-9A-Z]{16}',
				'AD'=>'[0-9]{8}[0-9A-Z]{12}',
				'AT'=>'[0-9]{16}',
				'BE'=>'[0-9]{12}',
				'BA'=>'[0-9]{16}',
				'BG'=>'[A-Z]{4}[0-9]{6}[0-9A-Z]{8}',
				'HR'=>'[0-9]{17}',
				'CY'=>'[0-9]{8}[0-9A-Z]{16}',
				'CZ'=>'[0-9]{20}',
				'DK'=>'[0-9]{14}',
				'EE'=>'[0-9]{16}',
				'FO'=>'[0-9]{14}',
				'FI'=>'[0-9]{14}',
				'FR'=>'[0-9]{10}[0-9A-Z]{11}[0-9]{2}',
				'GE'=>'[0-9A-Z]{2}[0-9]{16}',
				'DE'=>'[0-9]{18}',
				'GI'=>'[A-Z]{4}[0-9A-Z]{15}',
				'GR'=>'[0-9]{7}[0-9A-Z]{16}',
				'GL'=>'[0-9]{14}',
				'HU'=>'[0-9]{24}',
				'IS'=>'[0-9]{22}',
				'IE'=>'[0-9A-Z]{4}[0-9]{14}',
				'IL'=>'[0-9]{19}',
				'IT'=>'[A-Z][0-9]{10}[0-9A-Z]{12}',
				'KZ'=>'[0-9]{3}[0-9A-Z]{3}[0-9]{10}',
				'KW'=>'[A-Z]{4}[0-9]{22}',
				'LV'=>'[A-Z]{4}[0-9A-Z]{13}',
				'LB'=>'[0-9]{4}[0-9A-Z]{20}',
				'LI'=>'[0-9]{5}[0-9A-Z]{12}',
				'LT'=>'[0-9]{16}',
				'LU'=>'[0-9]{3}[0-9A-Z]{13}',
				'MK'=>'[0-9]{3}[0-9A-Z]{10}[0-9]{2}',
				'MT'=>'[A-Z]{4}[0-9]{5}[0-9A-Z]{18}',
				'MR'=>'[0-9]{23}',
				'MU'=>'[A-Z]{4}[0-9]{19}[A-Z]{3}',
				'MC'=>'[0-9]{10}[0-9A-Z]{11}[0-9]{2}',
				'ME'=>'[0-9]{18}',
				'NL'=>'[A-Z]{4}[0-9]{10}',
				'NO'=>'[0-9]{11}',
				'PL'=>'[0-9]{24}',
				'PT'=>'[0-9]{21}',
				'RO'=>'[A-Z]{4}[0-9A-Z]{16}',
				'SM'=>'[A-Z][0-9]{10}[0-9A-Z]{12}',
				'SA'=>'[0-9]{2}[0-9A-Z]{18}',
				'RS'=>'[0-9]{18}',
				'SK'=>'[0-9]{20}',
				'SI'=>'[0-9]{15}',
				'ES'=>'[0-9]{20}',
				'SE'=>'[0-9]{20}',
				'CH'=>'[0-9]{5}[0-9A-Z]{12}',
				'TN'=>'[0-9]{20}',
				'TR'=>'[0-9]{5}[0-9A-Z]{17}',
				'AE'=>'[0-9]{19}',
				'GB'=>'[A-Z]{4}[0-9]{14}'
			);
			/*verifiate minimal length*/
			if(strlen($iban) < 14)
			{
				return 0;
			}

			/*Get country ISO*/
			$ctr = substr($iban,0,2);
			if(isset($rules[$ctr]) === false)
			{
				return 0;
			}
			/*Get validation rules by Country*/
			$check = substr($iban,4);

			/*if rules not match, Iban is false*/
			if(preg_match('~'.$rules[$ctr].'~',$check) !== 1)
			{
				return 0;
			}

			/*get string for validation calculation*/
			$check = $check.substr($iban,0,4);

			/*update character by decimal number*/
			$check = str_replace(
				array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T','U', 'V', 'W', 'X', 'Y', 'Z'),
				array('10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35'),
				$check
			);

			/*Final verification*/
			if (bcmode($check,97) != 1) {
				return 0;
			}
			else{
				return 1;
			}
}

function arrayByHours($tabAppoints) {

	$tabHours = [];
	foreach($tabAppoints as $appoint) {

		$appointDate = $appoint[0];
		$duration = $appoint[1];

		array_push($tabHours, $appointDate); // first hour of appoint (in any case)

		if ($duration > 1) { // if appoint last more than one hour
			$appointTime = explode(' ', $appointDate)[1]; // get time part in the string date
			$appointHours = explode(':', $appointTime)[0]; // get hours in the string time
			$appointHours = (int) $appointHours;
			for ($i = 1; $i < $duration; $i++) {
				$hours = (string) ($appointHours + $i);
				$hours = explode(' ', $appointDate)[0] . ' '.$hours.':00:00';
				array_push($tabHours, $hours);
			}
		}
	}
	return $tabHours;
}
