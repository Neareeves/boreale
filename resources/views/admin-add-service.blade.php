@extends('default')

@section('title')
	Addition d'une prestation
@endsection

@section('content')
<main id="add-service">
	<div id="user-header">
		<p>Bonjour, {{ session('fname') }} </p>
	</div>

	<h2>Addition de prestation</h2>
	<form method="post" action="{{ url('espace-admin/prestations/insertion') }}" enctype="multipart/form-data" id="service-form">
		@csrf
		<input type="text" name="title" placeholder="Titre">
		<span id="error-title"></span>
		<textarea name="description" placeholder="Description"></textarea>
		<span id="error-desc"></span>
		<input type="text" name="price" placeholder="Prix">
		<span id="error-price"></span>
		<input type="file" name="picture">

		<input type="submit" name="submit-service">
	</form>
</main>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('/js/service-validation.js') }}"></script>
@endsection
