@extends('default')

@section('title')
	Gestion des prestations
@endsection

@section('content')

<main id="services">

    <div id="user-header">
        <p>Bonjour, {{ session('fname') }} </p>
    </div>

	<a href="{{ url('/espace-admin/prestations/addition') }}">
		<button id="add-service">Ajouter une prestation</button>
	</a>
	<ul>
	@foreach ($services as $service)
		<li>
			<h5>
				{!! $service -> title !!}
				<a href="{{ url('/espace-admin/prestations/edition?id=') }}{{ $service -> id_service }}">
					<button class="edit-service">Editer</button>
				</a>
				<button class="delete-service" data-id="{{ $service -> id_service }}">Supprimer</button>
			</h5>
			<p>Description : {!! $service -> description !!}</p>
			<p>Prix : {!! $service -> price !!}</p>
			@if ($service -> picture !== '')
				<img src="{{ asset('/img/services') }}/{{ $service -> picture }}">
			@endif
		</li>
	@endforeach
	</ul>

	<div class="overlay">
		<div class="window">
			<p>Etes-vous sûr(e) de vouloir supprimer cette prestation ?</p>
			<button id="no">Non</button>
			<a id="yes" href="{{ url('/espace-admin/prestations/suppression?id=') }}">
				<button>Oui</button>
			</a>
		</div>
	</div>

</main>

@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('/js/services.js') }}"></script>
@endsection
