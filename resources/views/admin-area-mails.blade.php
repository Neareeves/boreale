@extends('default')

@section('title')
	Espace administrateur
@endsection

@section('content')
<main id="admin-area-mails">
	<div id="user-header">
		<p>Bonjour, <span>{{ session('fname') }}</span> </p>
	</div>

  <section>
		<div id="filter-btns">
			<button>Validation patients</button>
			<button>Validation praticiens</button>
			<button>Validation admins</button>
			<button>Contact</button>
			<button>Mot de passe perdu</button>
		</div>

		<div id="area">
			<form>
				<p id="title-gui">Mail validation de compte (patient)</p>

				<input type="text" name="object" maxlength="42" placeholder="objet" value="{{ $mail['object'] }}">
				<textarea name="mail" placeholder="mail">{{ $mail['message'] }}</textarea>
				<button name="save-btn">Enregistrer</button>

			</form>
		</div>

	</section>

</main>
@endsection

@section('scripts')
	<script type="text/javascript" src="{{ asset('/js/mails.js') }}"></script>
@endsection