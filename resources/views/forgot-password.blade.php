@extends('default')

@section('title')
Inscription | Boréale
@endsection

@section('content')

@if ($popup != false)
	<div class="overlay visible">
		<div class="window">
			<p>{{ $popup }}</p>
			<a href="/"><button>retour à l'accueil</button></a>
		</div>
	</div>
@endif

<main>
	<section class="gui">
		<p>Saisissez votre adresse email.
		Un lien de récupération vous sera envoyé par email.</p>
		<form method="post" action="{{ url('oubli') }}" id="forgot-password">
			@csrf

			<input type="email" name="mail" placeholder="Adresse email">
			<span id="error-mail"></span>

			<input class="inpBtn" type="submit" name="submit-btn" value="Valider">
		</form>
	</section>
</main>

@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('/js/forgot.js') }}"></script>
@endsection
