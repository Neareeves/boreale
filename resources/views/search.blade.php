@extends('default')

@section('title')
Recherche de prestations
@endsection

@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"
	integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
	crossorigin=""/>
<link rel="stylesheet" type="text/css" href="{{ asset('/css/search.css') }}">
@endsection

@section('content')

<main id="search">
	<div id="user-header">
		<p>Bonjour, <span>{{ session('fname') }}</span></p>
	</div>

	<q class="editable">{{ $texts[0] }}</q>
	@if (session('type') == 'admin')
		<div id="edit-group0" class="hidden edit-area">
			<textarea class="edit-textarea" name="edit-area0" data-title="0">{{ $texts[0] }}</textarea>
			<button type="button" id='edit-cancel0' class="edit-cancel">annuler</button>
			<button type="button" id="edit-valid0" class="edit-valid">valider</button>
		</div>
		<div id="edit-icon0" class="edit-icon"></div>
	@endif

	<section id="map-container">
		<header class="mid-page-header">
				<h3 class="editable">{{ $texts[1] }}</h3>
				@if (session('type') == 'admin')
					<div id="edit-group1" class="hidden edit-area">
						<textarea class="edit-textarea" name="edit-area1" data-title="1">{{ $texts[1] }}</textarea>
						<button type="button" id='edit-cancel1' class="edit-cancel">annuler</button>
						<button type="button" id="edit-valid1" class="edit-valid">valider</button>
					</div>
					<div id="edit-icon1" class="edit-icon"></div>
				@endif
		</header>

		<div id="map"></div>

		<section id="practitioners">
			<h2>Praticiens à proximité</h2>
		</section>

		<div id="error" class="overlay">
			<div class="window">
				<p>Votre adresse ne permet pas de vous localiser. Veuillez vérifier son format.</p>
				<p>Si le problème persiste, contactez un administrateur.</p>
				<a href="{{ url('/espace-patient/profil') }}">
					<button>Retour au profil</button>
				</a>
			</div>
		</div>
	</section>
</main>

@endsection

@section('scripts')
<script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"
	integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg=="
	crossorigin=""></script>
<script type="text/javascript" src="{{ asset('/js/search.js') }}"></script>
@endsection
