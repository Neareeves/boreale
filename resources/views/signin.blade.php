@extends('default')

@section('title')
Connexion | Boréale
@endsection

@section('content')

{{ $advice }}

<div id="main-signin">
	<section>
		<form id="login-form" method="get" action="{{ url('connexion') }}">
			@csrf
			<input type="text" id="email" name="email" placeholder="Adresse email">

			<input type="password" id="password" name="password" placeholder="Mot de passe">

			<input class="inpBtn" type="submit" id="signin-button" name="signin-button" value="{{__('Connexion')}}">
		</form>
		<nav>
			<a href="{{ url('inscription') }}">Pas de compte ?</a>
			<a href="{{ url('oubli') }}">Mot de passe oublié ?</a>
		</nav>
	</section>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/signin.js') }}"></script>
@endsection
