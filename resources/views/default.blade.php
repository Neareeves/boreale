<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="Léo Briet, Estelle Jacquet, Yoann Moreau et Cantin Poiseau">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>@yield('title')</title>
	<link rel="stylesheet" href="{{ asset('/css/app.css') }}" type="text/css">
	@yield('css')
	<link rel="icon" href="{{ asset('/img/favicon.ico')}}">
</head>

<body>

	<header id="main-header">
		<nav>
			<a href="{{ url('/') }}" id="logo-boreale">
				<div class="logo-boreale-face"></div>
				<div class="logo-boreale-flower"></div>
			</a>

			<h1><a href="{{ url('/') }}">Boréale</a></h1>

			<div class="menus-container">
				@if (session('user'))
					<div class="icon-user" id="user-icon"></div>
				@else
					<a href="{{ url('connexion') }}">
						<div class="icon-user"></div>
					</a>
				@endif
				@component('inc.burger')
				@endcomponent
			</div>
		</nav>
	</header>
	@component('inc.burger-menu')
	@endcomponent
	@component('inc.user-menu')
	@endcomponent

	@yield('content')

	<footer id="main-footer">
		<nav>
			<div id="footer-links">
				<a href="{{ url('partenaires') }}">Nos partenaires</a>
				<a href="{{ url('mentions-legales') }}">Mentions Légales - CGU</a>
				<a href="{{ url('faq') }}">F.A.Q.</a>
			</div>
			<div id="social-medias">
				<a href="#">
					<div id="icon-facebook"></div>
				</a>
				<a href="#">
					<div id="icon-twitter"></div>
				</a>
				<a href="#">
					<div id="icon-instagram"></div>
				</a>
			</div>
		</nav>
	</footer>

	<script type="text/javascript" src="{{ asset('/js/global.js') }}"></script>
	@if (session('type') == 'admin')
		<script type="text/javascript" src="{{ asset('/js/edition.js') }}"></script>
	@endif
 	@yield('scripts')
</body>

</html>
