@extends('default')

@section('title')
    Informations patients
@endsection

@section('content')

<main id="patient-infos">
	<section>
		<header class="page-header">
		</header>
	</section>
	<section class="infos">
		<header class="mid-page-header">
			<h3 class="editable">{{ $texts[0] }}</h3>
			@if (session('type') == 'admin')
			<div id="edit-group0" class="hidden edit-area">
				<textarea class="edit-textarea" name="edit-area2" data-title="0">{{ $texts[0] }}</textarea>
				<button type="button" id='edit-cancel0' class="edit-cancel">annuler</button>
				<button type="button" id="edit-valid0" class="edit-valid">valider</button>
			</div>
			<div id="edit-icon0" class="edit-icon"></div>
			@endif
		</header>
		<article class="editable">{{ $texts[1] }}</article>
		@if (session('type') == 'admin')
		<div id="edit-group1" class="hidden edit-area">
			<textarea class="edit-textarea" name="edit-area1" data-title="1">{{ $texts[1] }}</textarea>
			<button type="button" id='edit-cancel1' class="edit-cancel">annuler</button>
			<button type="button" id="edit-valid3" class="edit-valid">valider</button>
		</div>
		<div id="edit-icon1" class="edit-icon"></div>
		@endif
		<article class="editable">{{ $texts[2] }}</article>
		@if (session('type') == 'admin')
		<div id="edit-group2" class="hidden edit-area">
			<textarea class="edit-textarea" name="edit-area2" data-title="2">{{ $texts[2] }}</textarea>
			<button type="button" id='edit-cancel2' class="edit-cancel">annuler</button>
			<button type="button" id="edit-valid2" class="edit-valid">valider</button>
		</div>
		<div id="edit-icon2" class="edit-icon"></div>
		@endif
		<img src="{{ asset('/img/manucure.jpg') }}" alt="Image massage">
		<article class="editable">{{ $texts[3] }}</article>
		@if (session('type') == 'admin')
		<div id="edit-group3" class="hidden edit-area">
			<textarea class="edit-textarea" name="edit-area3" data-title="3">{{ $texts[3] }}</textarea>
			<button type="button" id='edit-cancel3' class="edit-cancel">annuler</button>
			<button type="button" id="edit-valid3" class="edit-valid">valider</button>
		</div>
		<div id="edit-icon3" class="edit-icon"></div>
		@endif
		<article class="editable">{{ $texts[4] }}</article>
		@if (session('type') == 'admin')
		<div id="edit-group4" class="hidden edit-area">
			<textarea class="edit-textarea" name="edit-area4" data-title="4">{{ $texts[4] }}</textarea>
			<button type="button" id='edit-cancel4' class="edit-cancel">annuler</button>
			<button type="button" id="edit-valid4" class="edit-valid">valider</button>
		</div>
		<div id="edit-icon4" class="edit-icon"></div>
		@endif
		<a  href="{{url('/inscription')}}"><button class="inpBtn" id="inscription-button" name="inscription-button">{{__('Je m\'inscris')}}</button></a>
	</section>
</main>

@endsection

@section('scripts')
@endsection
