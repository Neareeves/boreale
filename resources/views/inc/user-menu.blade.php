<div class="menu-container">
	<nav class="user-menu">
		@if(session('type') === 'patient')
			<a href="{{ url('/espace-patient') }}">Mon espace</a>
			<a href="{{ url('/espace-patient/recherche') }}">Recherche de services</a>
			<a href="{{ url('/espace-patient/profil') }}">Mon profil</a>
			<a href="{{ url('/espace-patient/agenda') }}">Mon agenda</a>
		@endif

		@if(session('type') === 'practitioner')
			<a href="{{ url('/espace-praticien') }}">Mon espace</a>
			<a href="{{ url('/espace-praticien/agenda') }}">Mon agenda</a>
			<a href="{{ url('/espace-praticien/prestations') }}">Gestion des prestations</a>
			<a href="{{ url('/espace-praticien/profil') }}">Mon profil</a>
			<a href="{{ url('/espace-praticien/factures') }}">Mes factures</a>
		@endif

		@if(session('type') === 'admin')
			<a href="{{ url('/espace-admin') }}">Mon espace</a>
			<a href="{{ url('/espace-admin/factures') }}">Comptabilité</a>
			<a href="{{ url('/espace-admin/prestations') }}">Gestion des prestations</a>
			<a href="{{ url('/espace-admin/mails') }}">Gestion des mails</a>
			<a href="{{ url('/espace-admin/editions') }}">Edition des pages</a>

			@if (session('super') === true)
				<a href="{{ url('/espace-admin/admins') }}">Les admins</a>
			@endif
			<a href="{{ url('/espace-admin/profil') }}">Mon profil</a>
		@endif

		<a href="{{ url('deconnexion') }}">Déconnexion</a>
	</nav>
</div>