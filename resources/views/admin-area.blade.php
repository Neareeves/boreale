@extends('default')

@section('title')
	Espace administrateur
@endsection

@section('content')
<main id="admins">
    <div id="user-header">
        <p>Bonjour, <span>{{ session('fname') }}</span> </p>
    </div>

    <q class="editable">{{ $texts[0] }}</q>
    @if (session('type') == 'admin')
			<div id="edit-group0" class="hidden edit-area">
				<textarea class="edit-textarea" name="edit-area0" data-title="0">{{ $texts[0] }}</textarea>
				<button type="button" id='edit-cancel0' class="edit-cancel">annuler</button>
				<button type="button" id="edit-valid0" class="edit-valid">valider</button>
			</div>
			<div id="edit-icon0" class="edit-icon"></div>
		@endif

    <nav>
			<a href="{{ url('/espace-admin/compta') }}" class="nav-area-btn">G&eacute;rer la comptabilit&eacute;</a>
			<a href="{{ url('/espace-admin/profil') }}" class="nav-area-btn">G&eacute;rer mon profil</a>
			<a href="{{ url('/espace-admin/prestations') }}" class="nav-area-btn">G&eacute;rer les pr&eacute;stations</a>
			<a href="{{ url('/espace-admin/mails') }}" class="nav-area-btn">G&eacute;rer les mails</a>
			<a href="{{ url('/espace-admin/editions') }}" class="nav-area-btn">Editer des pages</a>
			@if (session('super') == true)
				<a href="{{ url('/espace-admin/admins') }}" class="nav-area-btn">G&eacute;rer les administrateurs</a>
			@endif
    </nav>
</main>
@endsection

@section('scripts')
@endsection
