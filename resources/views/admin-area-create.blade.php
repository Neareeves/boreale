@extends('default')

@section('title')
	Espace administrateur
@endsection

@section('content')
<main id="admin-admins">
    <div id="user-header">
        <p>Bonjour, {{ session('fname') }} </p>
    </div>
</main>

<section>
		<form method="post" action="{{ url('espace-admin/creation-compte-admin') }}" id="admin-creation">
			@csrf

			<span class="trait"><input type="email" name="mail" placeholder="Adresse email"></span>
			<span id="error-mail"></span>

			<span class="trait"><input type="text" name="firstname" maxlength="50" placeholder="Prénom"></span>
			<span id="error-fname"></span>

			<span class="trait"><input type="text" name="lastname" maxlength="50" placeholder="Nom"></span>
			<span id="error-lname"></span>

			<input class="inpBtn" type="submit" name="submit-btn" value="Créer un compte admin.">
		</form>
	</section>

@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('/js/signup-admin.js') }}"></script>
@endsection
