@extends('default')

@section('title')
	Contact
@endsection

@section('content')

<main id="contact">
	<header class="page-header">
	</header>

	<header class="mid-page-header">

		<h3 class="editable">{{ $texts[0] }}</h3>
		@if (session('type') == 'admin')
			<div id="edit-group0" class="hidden edit-area">
				<textarea class="edit-textarea" name="edit-area0" data-title="0">{{ $texts[0] }}</textarea>
				<button type="button" id='edit-cancel0' class="edit-cancel">annuler</button>
				<button type="button" id="edit-valid0" class="edit-valid">valider</button>
			</div>
			<div id="edit-icon0" class="edit-icon"></div>
		@endif
	</header>
	<section class="gui">
		<form method="post" action="{{ url('contact/envoi') }}" id="contact-form">
			@csrf
			<label for="name"></label>
			<input name="name" type="text" id="name" class="inpText form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name') }}" required autofocus placeholder="{{__('Nom :')}}">
			<label for="first-name"></label>
			<input name="first-name" type="text" id="first-name" class="inpText form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}"  value="{{ old('first-name') }}" required placeholder="{{__('Prénom :')}}">
			<label for="email"></label>
			<input name="email" type="email" id="email" class="inpText form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"  value="{{ old('email') }}" required placeholder="{{__('Email :')}}">
			<label for="subject"></label>
			<input name="subject" type="text" id="subject" class="inpText form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}"  value="{{ old('subject') }}" required placeholder="{{__('Sujet :')}}">
			<textarea name="msg" id="msg" class="inpText form-control{{ $errors->has('msg') ? ' is-invalid' : '' }}" placeholder="{{__('Message :')}}" required></textarea>
			<input class="inpBtn" type="submit" name="submit-btn" value="{{__('Envoyer :')}}">
		</form>
	</section>
</main>
@endsection
