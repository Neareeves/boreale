@extends('default')

@section('title')
    Informations praticien
@endsection

@section('content')

<main id="practitioner-infos">
	<section>
		<header class="page-header">
		</header>
	</section>
	<section class="infos">
		<header class="mid-page-header">
			<h3 class="editable">{{ $texts[0] }}</h3>
			@if (session('type') == 'admin')
			<div id="edit-group0" class="hidden edit-area">
				<textarea class="edit-textarea" name="edit-area0" data-title="0">{{ $texts[0] }}</textarea>
				<button type="button" id='edit-cancel0' class="edit-cancel">annuler</button>
				<button type="button" id="edit-valid0" class="edit-valid">valider</button>
			</div>
			<div id="edit-icon0" class="edit-icon"></div>
			@endif
		</header>
		<article class="editable">{{ $texts[1] }}</article>
		@if (session('type') == 'admin')
		<div id="edit-group1" class="hidden edit-area">
			<textarea class="edit-textarea" name="edit-area1" data-title="1">{{ $texts[1] }}</textarea>
			<button type="button" id='edit-cancel1' class="edit-cancel">annuler</button>
			<button type="button" id="edit-valid1" class="edit-valid">valider</button>
		</div>
		<div id="edit-icon1" class="edit-icon"></div>
		@endif
		<article class="editable">{{ $texts[2] }}</article>
		@if (session('type') == 'admin')
		<div id="edit-group2" class="hidden edit-area">
			<textarea class="edit-textarea" name="edit-area2" data-title="2">{{ $texts[2] }}</textarea>
			<button type="button" id='edit-cancel2' class="edit-cancel">annuler</button>
			<button type="button" id="edit-valid2" class="edit-valid">valider</button>
		</div>
		<div id="edit-icon2" class="edit-icon"></div>
		@endif
		<article class="editable">{{ $texts[3] }}</article>
		@if (session('type') == 'admin')
		<div id="edit-group3" class="hidden edit-area">
			<textarea class="edit-textarea" name="edit-area3" data-title="3">{{ $texts[3] }}</textarea>
			<button type="button" id='edit-cancel3' class="edit-cancel">annuler</button>
			<button type="button" id="edit-valid3" class="edit-valid">valider</button>
		</div>
		<div id="edit-icon3" class="edit-icon"></div>
		@endif
		<a  href="{{url('/inscription')}}"><button class="inpBtn" id="inscription-button" name="inscription-button">{{__('Ça m\'intéresse')}}</button></a>
	</section>
</main>

@endsection

@section('scripts')
@endsection
