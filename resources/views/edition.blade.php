@extends('default')

@section('title')
	Edition de pages
@endsection

@section('content')
<main id="edition">
    <div id="user-header">
        <p>Bonjour, <span>{{ session('fname') }}</span> </p>
    </div>

    <q class="editable">{{ $texts[0] }}</q>
    @if (session('type') == 'admin')
			<div id="edit-group0" class="hidden edit-area">
				<textarea class="edit-textarea" name="edit-area0" data-title="0">{{ $texts[0] }}</textarea>
				<button type="button" id='edit-cancel0' class="edit-cancel">annuler</button>
				<button type="button" id="edit-valid0" class="edit-valid">valider</button>
			</div>
			<div id="edit-icon0" class="edit-icon"></div>
		@endif

		<h4>Pages générales du site</h4>
    <nav>
			<a href="{{ url('/') }}" class="nav-edition-btns">Accueil</a>
			<a href="{{ url('/informations-praticien') }}" class="nav-edition-btns">Informations praticien</a>
			<a href="{{ url('/informations-patient') }}" class="nav-edition-btns">Informations patient</a>
			<a href="{{ url('/presentation') }}" class="nav-edition-btns">Qui sommes-nous ?</a>
			<a href="{{ url('/contact') }}" class="nav-edition-btns">Contact</a>
    	<a href="{{ url('/partenaires') }}" class="nav-edition-btns">Partenaires</a>
    	<a href="{{ url('/mentions-legales') }}" class="nav-edition-btns">Mentions Légales</a>
    	<a href="{{ url('/faq') }}" class="nav-edition-btns">F.A.Q</a>
    </nav>

    <h4>Espace patient</h4>
    <nav>
			<a href="{{ url('/espace-patient') }}" class="nav-edition-btns">Espace patient</a>
			<a href="{{ url('/espace-patient/recherche') }}" class="nav-edition-btns">Recherche de prestations</a>
			<a href="{{ url('/espace-patient/profil') }}" class="nav-edition-btns">Profil patient</a>
			<a href="{{ url('/espace-patient/agenda') }}" class="nav-edition-btns">Agenda patient</a>
    </nav>

    <h4>Espace praticien</h4>
    <nav>
			<a href="{{ url('/espace-praticien') }}" class="nav-edition-btns">Espace praticien</a>
			<a href="{{ url('/espace-praticien/prestations') }}" class="nav-edition-btns">Gestion des prestations</a>
			<a href="{{ url('/espace-praticien/profil') }}" class="nav-edition-btns">Profil praticien</a>
			<a href="{{ url('/espace-praticien/agenda') }}" class="nav-edition-btns">Agenda praticien</a>
			<a href="{{ url('/espace-praticien/factures') }}" class="nav-edition-btns">Gestion des factures</a>
    </nav>
</main>
@endsection

@section('scripts')
@endsection
