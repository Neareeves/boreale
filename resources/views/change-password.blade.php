@extends('default')

@section('title')
Récupération mot de passe
@endsection

@section('content')

@if ($popup != false)
	<div class="overlay visible">
		<div class="window">
		<p>{{ $popup }}</p>
		<a href="/"><button>retour à l'accueil</button></a>
		</div>
	</div>
@endif

<main>
	<section class="gui">
		<form method="post" action="{{ $link }}" id="change-pass">
			@csrf

			<input type="password" name="password" placeholder="Mot de passe">
			<span id="error-pw"></span>

			<input type="password" name="confirm-password" placeholder="Confirmer mot de passe">
			<span id="error-cpw"></span>

			<input class="inpBtn" type="submit" name="submit-btn" value="modifier le mot de passe">
		</form>
	</section>
</main>

@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('/js/change.js') }}"></script>
@endsection
