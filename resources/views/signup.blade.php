@extends('default')

@section('title')
Inscription | Boréale
@endsection

@section('content')

@if ($popup !== false)
<div class="overlay visible">
	<div class="window">
		<p>{{ $popup }}</p>
		<a href="{{ url('/') }}"><button>Retour à l'accueil</button></a>
	</div>
</div>
@endif

<main>

	<section id='inscription'>
		<form method="post" action="{{ url('creation-compte') }}" id="account-creation">
			@csrf
			<article class="choice">
				<input type="radio" name="account-type" value="patient" id="radio-patient" checked>
				<label for="radio-patient">Je suis patient(e)</label>

				<input type="radio" name="account-type" value="practitioner" id="radio-practitioner">
				<label for="radio-practitioner">Je suis praticien(ne)</label>
			</article>

			<span class="trait"><input type="email" name="mail" placeholder="Adresse email"></span>
			<span id="error-mail"></span>

			<span class="trait"><input type="password" name="password" placeholder="Mot de passe"></span>
			<span id="error-pw"></span>

			<span class="trait"><input type="password" name="confirm-password" placeholder="Confirmer mot de passe"></span>
			<span id="error-cpw"></span>

			<span class="trait"><input type="text" name="firstname" maxlength="50" placeholder="Prénom"></span>
			<span id="error-fname"></span>

			<span class="trait"><input type="text" name="lastname" maxlength="50" placeholder="Nom"></span>
			<span id="error-lname"></span>

			<span class="trait"><input type="text" name="address" placeholder="Adresse"></span>
			<span id="error-address"></span>

			<span class="trait"><input type="text" name="zip-code" maxlength="5" placeholder="Code postal"></span>
			<span id="error-zcode"></span>

			<span class="trait"><input type="text" name="city" placeholder="Ville"></span>
			<span id="error-city"></span>

			<input class="inpBtn" type="submit" name="submit-btn" value="S'inscrire">
		</form>
		<div class="other_page">
			<a href="{{ url('connexion') }}">{{__("J'ai déjà un compte.")}}</a>
		</div>
	</section>
</main>

@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('/js/signup.js') }}"></script>
@endsection
